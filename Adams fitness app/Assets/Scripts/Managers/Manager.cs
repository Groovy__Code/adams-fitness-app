﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;
using System.Security.Cryptography;
using Michsky.UI.ModernUIPack;
using System.IO;

public class Manager : MonoBehaviour
{
    public static Manager Instance;

    [HideInInspector] public TaskObject[] tasks = new TaskObject[7];
    [SerializeField] private Sprite[] TaskIcons;

    string[] taskType = { "Workout", "Yoga", "Rest" };
    string[] preMadeTasks = new string[] { "Workout", "Workout", "Workout", "Yoga", "Yoga", "Rest", "Rest" };
    string[] videoLinks = { "https://youtu.be/2pLT-olgUJs", "https://youtu.be/sTANio_2E0Q", null };

    [SerializeField] private GameObject[] taskGameObjects;
    [SerializeField] private Button startActivityButton;
    [SerializeField] private Button toggleComplete;
    [SerializeField] private TextMeshProUGUI buttonText;
    [SerializeField] private TextMeshProUGUI countdown;

    int seed;
    Random random = new Random();
    int randNumber;

    DateTime dateTime;

    

    private void Awake()
    {        
        if (Instance != null)
        {
            Debug.Log("You already have an instance");
            return;
        }
        else Instance = this;

        Screen.orientation = ScreenOrientation.Portrait;
    }

    void Start()
    {
        //random = new Random(seed);
        GetTxtFile();
        
        UpdateTaskDisplay();
    }

    void GetTxtFile()
    {
        var file = Resources.Load<TextAsset>("Task file");

        if (file == null)
        {
            GenerateTasks();
        }
        else
        {
            FormatFile();
        }
    }

    void FormatFile()
    {

    }

    void Update()
    {

    }
    
    string[] Shuffle(string[] array)
    {
        int p = array.Length;
        for (int n = p - 1; n > 0; n--)
        {
            int r = random.Next(1, n);
            string t = array[r];
            array[r] = array[n];
            array[n] = t;
        }
        return array;
    }

    void GenerateTasks()
    {        
        Shuffle(preMadeTasks);

        for (int i = 0; i <= 6; i++)
        {
            string link;
            Sprite icon;
                        
            if (preMadeTasks[i] == taskType[0])
            {
                link = videoLinks[0];
                icon = TaskIcons[0];
            }
            else if (preMadeTasks[i] == taskType[1])
            {
                link = videoLinks[1];
                icon = TaskIcons[1];
            }
            else
            {
                link = videoLinks[2];
                icon = TaskIcons[2];
            }

            if (i == 0)
            {
                tasks[i] = new TaskObject(preMadeTasks[i],DateTime.Today,link,icon);
            }
            else
            {
                tasks[i] = new TaskObject(preMadeTasks[i], tasks[i - 1].taskDateSet.AddDays(1), link, icon);
            }

            CheckSequence(i);

            //randNumber = random.Next(0, taskType.Length);
            //if (i == 0)
            //{
            //    tasks[i] = new TaskObject($"{taskType[randNumber]}", DateTime.Today, videoLinks[randNumber], TaskIcons[randNumber]);
            //}
            //else
            //{
            //    tasks[i] = new TaskObject($"{taskType[randNumber]}", tasks[i - 1].taskDateSet.AddDays(1), videoLinks[randNumber], TaskIcons[randNumber]);
            //}
        }
    }

    void CheckSequence(int i)
    {
        try
        {
            if (tasks[i].taskTitle == taskType[2] && tasks[i-1].taskTitle == taskType[2])
            {
                TaskObject temp;
                for (int j = tasks.Length; j > 0; j--)
                {
                    if (tasks[j].taskTitle == taskType[0])
                    {
                        temp = tasks[j];
                        tasks[j] = tasks[i];
                        tasks[i] = temp;
                    }
                }
            }
        }
        catch
        {

        }
    }

    public void UpdateTaskDisplay()
    {
        for (int i = 0; i < tasks.Length; i++)
        {
            TextMeshProUGUI Title = taskGameObjects[i].transform.Find("TaskType").GetComponent<TextMeshProUGUI>();
            Title.text = tasks[i].taskTitle;

            Image image = taskGameObjects[i].transform.Find("ActivityIcon").GetComponent<Image>();
            image.sprite = tasks[i].image;

            if (i > 1)
            {
                TextMeshProUGUI Date = taskGameObjects[i].transform.Find("DailyText").GetComponent<TextMeshProUGUI>();
                Date.text = tasks[i].taskDateSet.DayOfWeek.ToString();
            }
        }

        startActivityButton.interactable = true;

        if (tasks[0].taskTitle == taskType[0])
        {
            buttonText.text = "Start workout";
        }
        else if (tasks[0].taskTitle == taskType[1])
        {
            buttonText.text = "Start yoga routine";
        }
        else
        {
            buttonText.text = "No activity";            
            startActivityButton.interactable = false;
        }
    }

    public void TaskComplete()
    {
        for (int i = 1; i < tasks.Length; i++)
        {
            tasks[i - 1] = tasks[i];
        }

        string[] tempTasks = Shuffle(preMadeTasks);
        tasks[6] = new TaskObject(tempTasks[0], tasks[5].taskDateSet.AddDays(1), videoLinks[randNumber], TaskIcons[randNumber]);

        UpdateTaskDisplay(); 
    }

    int timer = 3;
    public IEnumerator TestCoolDown()
    {
        countdown.gameObject.SetActive(true);
        while (timer > 0)
        {            
            countdown.text = $"New activity in: {timer}";
            timer--;
            yield return new WaitForSeconds(1f);
        }
        countdown.gameObject.SetActive(false);
        timer = 3;

        //CalanderManager.Instance.UpdateCompletedDays(true);

    }

    public IEnumerator CountdownTillNextActivity()
    {
        countdown.gameObject.SetActive(true);

        TimeSpan untilMidnight = DateTime.Today.AddDays(1) - DateTime.Now;
        double secs = untilMidnight.TotalSeconds;

        while (secs > 0)
        {
            untilMidnight = DateTime.Today.AddDays(1) - DateTime.Now;
            secs = untilMidnight.TotalSeconds;

            var ts = TimeSpan.FromSeconds(secs);            
            countdown.text = $"New activity in: {string.Format($"{ts.Hours}:{ts.Minutes}:{ts.Seconds}")}";

            yield return new WaitForSeconds(1f);
        }
        countdown.gameObject.SetActive(false);

        CalanderManager.Instance.UpdateCompletedDays(true);
        
    }
    
}

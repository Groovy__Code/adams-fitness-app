﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalanderManager : MonoBehaviour
{
    public static CalanderManager Instance;

    [SerializeField] private GameObject Calender;
    RectTransform rec;

    [SerializeField] private TextMeshProUGUI month;
    [SerializeField] private Button leftNavigationArrow;
    [SerializeField] private Button rightNavigationArrow;

    [SerializeField] private GameObject[] days;

    private Dictionary<DateTime, bool> completedDays = new Dictionary<DateTime, bool>();

    DateTime currentDateTime;
    DateTime currentDisplayedDateTime;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.Log("You already have an instance");
            return;
        }
        else Instance = this;
    }

    void Start()
    {
        rec = Calender.GetComponent<RectTransform>();

        currentDateTime = DateTime.Now;
        month.text = currentDateTime.ToString("MMMM yyyy");

        currentDisplayedDateTime = currentDateTime;

        leftNavigationArrow.onClick.AddListener(delegate { ChangeMonth(-1); });
        rightNavigationArrow.onClick.AddListener(delegate { ChangeMonth(1); });

        UpdateCalander();
    }

    void ChangeMonth(int direction)
    {
        currentDisplayedDateTime = currentDisplayedDateTime.AddMonths(direction);
        month.text = currentDisplayedDateTime.ToString("MMMM yyyy");
        //Debug.Log(DateTime.DaysInMonth(currentDisplayedDateTime.Year, currentDisplayedDateTime.Month));
        UpdateCalander();
    }

    private string[] daysOfWeek = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

    DateTime tempDate;

    private void UpdateCalander()
    {

        string firstDayOfMonth = new DateTime(currentDisplayedDateTime.Year, currentDisplayedDateTime.Month, 1).DayOfWeek.ToString();

        int startIndex = 0;

        for (int i = 0; i < daysOfWeek.Length; i++)
        {
            if (daysOfWeek[i] == firstDayOfMonth)
            {
                startIndex = i;
                break;
            }
        }

        int year = currentDisplayedDateTime.Year;
        int month = currentDisplayedDateTime.Month;
        int daysInMonth = DateTime.DaysInMonth(year, month);
        int temp = 1;

        int iterate = 42;
        this.gameObject.transform.position = new Vector3(720, 2200f, 0);
        rec.sizeDelta = new Vector2(1300, 766);

        if (daysInMonth + startIndex <= 35)
        {
            iterate = 35;
            this.gameObject.transform.position = new Vector3(720, 2250f, 0);
            rec.sizeDelta = new Vector2(1300, 666);
        }
        else if (daysInMonth + startIndex == 28)
        {
            iterate = 28;
            this.gameObject.transform.position = new Vector3(720, 2300f, 0);
            rec.sizeDelta = new Vector2(1300, 566);
        }
                

        for (int i = 0; i < iterate; i++)
        {
            days[i].SetActive(true);

            TextMeshProUGUI text;
            text = days[i].transform.Find("Number of day in the week").GetComponent<TextMeshProUGUI>();

            Image image;
            image = days[i].GetComponent<Image>();            

            if (i < startIndex)
            {
                currentDisplayedDateTime = currentDisplayedDateTime.AddMonths(-1);
                text.text = new DateTime(currentDisplayedDateTime.Year, currentDisplayedDateTime.Month, DateTime.DaysInMonth(currentDisplayedDateTime.Year, currentDisplayedDateTime.Month) + i - startIndex + 1).ToString("dd");
                currentDisplayedDateTime = currentDisplayedDateTime.AddMonths(1);
                image.color = Color.gray;
            }
            else if (i - startIndex < daysInMonth)
            {
                DateTime date = new DateTime(year, month, i - startIndex + 1);
                text.text = date.ToString("dd");  

                if (DateTime.Today.Year == currentDisplayedDateTime.Year && DateTime.Today.Month == currentDisplayedDateTime.Month && DateTime.Today.Day == i - startIndex + 1)
                {
                    image.color = Color.blue;
                }
                else if (completedDays.TryGetValue(date, out bool value))
                {
                    if (value)
                    {
                        image.color = Color.green;
                    }
                    else
                    {
                        image.color = Color.red;
                    }
                }
                else
                {
                    image.color = Color.white;
                }
            }            
            else
            {
                string lastDayOfTheMonth = new DateTime(year, month, daysInMonth).DayOfWeek.ToString();
                int lastDayIndex = 0;

                for (int j = 0; j < daysOfWeek.Length; j++)
                {
                    if (daysOfWeek[j] == lastDayOfTheMonth)
                    {
                        lastDayIndex = j;
                        break;
                    }
                }

                text.text = (temp++).ToString();
                image.color = Color.gray;
            }

        }        
        

        if (iterate>=28)
        {
            for (int i = iterate; i < 42; i++)
            {
                days[i].SetActive(false);
            }
        }
    }

    int daysGone = 0;

    public void UpdateCompletedDays(bool completed)
    {
        currentDisplayedDateTime = DateTime.Today.AddDays(daysGone);
        Debug.Log(currentDisplayedDateTime);
        daysGone++;
        completedDays.Add(currentDisplayedDateTime, completed);
        currentDisplayedDateTime.AddDays(1);
        UpdateCalander();
    }
}

﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TaskComplete : MonoBehaviour
{
    Animator iconAnimator;
    Button eventButton;
    bool isClicked;

    [SerializeField] private TextMeshProUGUI completedText;

    Coroutine resetButton;

    TimeSpan untilMidnight;
    double secs;

    void Start()
    {
        untilMidnight = DateTime.Today.AddDays(1) - DateTime.Now;
        secs = untilMidnight.TotalSeconds;        

        iconAnimator = gameObject.GetComponent<Animator>();

        eventButton = gameObject.GetComponent<Button>();
        eventButton.onClick.AddListener(ClickEvent);
    }

    public void ClickEvent()
    {
        if (isClicked == true)
        {
            eventButton.interactable = true;
            iconAnimator.Play("Out");
            completedText.text = "Not done";
            isClicked = false;
        }
        else
        {
            eventButton.interactable = false;
            iconAnimator.Play("In");
            completedText.text = "Completed";
            isClicked = true;
        }

        if (resetButton != null)
        {
            StopCoroutine(resetButton);
            resetButton = null;
        }
        else
        {
            resetButton = StartCoroutine(ResetCompleteButton());                        
        }
    }

    IEnumerator ResetCompleteButton()
    {
        //untilMidnight = DateTime.Today.AddDays(1) - DateTime.Now;
        //secs = untilMidnight.TotalSeconds;

        //StartCoroutine(Manager.Instance.CountdownTillNextActivity());
        StartCoroutine(Manager.Instance.TestCoolDown());

        yield return new WaitForSeconds(3f);//(float)secs

        Manager.Instance.TaskComplete();
        ClickEvent();
    }
}

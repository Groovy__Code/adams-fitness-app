﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnButtonSelect : MonoBehaviour
{
    [SerializeField] private Button thisButton;
    
    public void ButtonSelected()
    {
        PageSelector.Instance.ButtonClicked(thisButton);
    }
}

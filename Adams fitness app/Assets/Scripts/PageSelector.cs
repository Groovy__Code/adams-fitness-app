﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageSelector : MonoBehaviour
{
    public static PageSelector Instance;

    [Header("Menu Icons")]
    [SerializeField] private Button progress;
    [SerializeField] private Button home;
    [SerializeField] private Button settings;

    [Header("Pages")]
    [SerializeField] private GameObject progressPage;
    [SerializeField] private GameObject homePage;
    [SerializeField] private GameObject settingsPage;
            
    Button currentActiveButton;

    [SerializeField] private GameObject canvas;

    void Start()
    {
        if (Instance != null)
        {
            Debug.Log("You already have an instance");
            return;
        }
        else Instance = this;

        homePage.transform.SetSiblingIndex(2);

        currentActiveButton = home;        
    }

    void OpenSelectedPage(string buttonName)
    {
        switch (buttonName)
        {
            case "Progress button":
                progressPage.transform.SetSiblingIndex(2);
                break;
            case "Home button":
                homePage.transform.SetSiblingIndex(2);
                break;
            case "Settings button":
                settingsPage.transform.SetSiblingIndex(2);
                break;
            default:
                break;
        }
    }

    public void ButtonClicked(Button button)
    {
        currentActiveButton.transform.Find("Selected").gameObject.SetActive(false);

        button.transform.Find("Selected").gameObject.SetActive(true);
        currentActiveButton = button;

        OpenSelectedPage(currentActiveButton.gameObject.name);
    }
}

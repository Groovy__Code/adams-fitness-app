﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskObject
{
    public string taskTitle;
    public DateTime taskDateSet;
    public string youtubeLink;
    public Sprite image;

    public TaskObject(string title, DateTime dateSet, string youtubeLink, Sprite image)
    {
        this.taskTitle = title;
        this.taskDateSet = dateSet;
        this.youtubeLink = youtubeLink;
        this.image = image;
    }
}
